package com.listi.LinkedList;

import com.listi.LinkedList.ArrayList2;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.List;

class ArrayList2Test {
        ArrayList2 cut = new ArrayList2(new Object[] {4, 19, 11, 68, 32, 45});

        @Test
        void sizeTest() {
            Object expected = 6;
            Object actual = cut.size();
            Assertions.assertEquals(expected, actual);
        }

      static List<Arguments> getTestArgs(){
            return List.of(
                    Arguments.arguments(3, 68),
                    Arguments.arguments(0, 4)
            );
        }

        @ParameterizedTest
        @MethodSource("getTestArgs")
        void getTest(int index, Object expected) {
            Object actual = cut.get(index);
            Assertions.assertEquals(expected, actual);
        }

        @Test
        void addTest() {
            boolean expected = true;
            boolean actual = cut.add(65);
            Assertions.assertEquals(expected, actual);
        }

       static   List<Arguments> addTestArgs(){
            return List.of(
                    Arguments.arguments(3, 22, true),
                    Arguments.arguments(123, 43, false)
            );
        }

        @ParameterizedTest
        @MethodSource("addTestArgs")
        void addTest(int index, Object value, boolean expected) {
            boolean actual = cut.add(index, value);
            Assertions.assertEquals(expected, actual);
        }

      static List<Arguments> removeTestArgs(){
            return List.of(
                    Arguments.arguments(4, true),
                    Arguments.arguments(2323, false)
            );
        }

        @ParameterizedTest
        @MethodSource("removeTestArgs")
        void removeTest(Object value, boolean expected) {
            boolean actual = cut.remove(value);
            Assertions.assertEquals(expected, actual);
        }

       static List<Arguments> removeByIndexTestArgs(){
            return List.of(
                    Arguments.arguments(4, 4),
                    Arguments.arguments(234, -1)
            );
        }

        @ParameterizedTest
        @MethodSource("removeByIndexTestArgs")
        void removeByIndexTest(int index,  int expected) {
            Object actual = cut.removeByIndex(index);
            Assertions.assertEquals(expected, actual);
        }

      static   List<Arguments> containsTestArgs(){
            return List.of(
                    Arguments.arguments(32, true),
                    Arguments.arguments(434, false)
            );
        }

        @ParameterizedTest
        @MethodSource("containsTestArgs")
        void containsTest(Object value, boolean expected) {
            boolean actual = cut.contains(value);
            Assertions.assertEquals(expected, actual);
        }

      static   List<Arguments> setTestArgs(){
            return List.of(
                    Arguments.arguments(0, 4, true),
                    Arguments.arguments(2323, 123, false)
            );
        }

        @ParameterizedTest
        @MethodSource("setTestArgs")
        void setTest(int index, Object value, boolean expected) {
            boolean actual = cut.set(index, value);
            Assertions.assertEquals(expected, actual);
        }

        @Test
        void toArrayTest() {
            Object[] expected = new Object[] {4, 19, 11, 68, 32, 45};
            Object[] actual = cut.toArray();
            Assertions.assertArrayEquals(expected, actual);
        }

       static List<Arguments> removeAllTestArgs(){
            return List.of(
                    Arguments.arguments(new Object[] {13, 232,2424}, false),
                    Arguments.arguments(new Object[] {4, 19, 11, 68, 32, 45}, true),
                    Arguments.arguments(new Object[] {1,20}, false)
            );
        }

        @ParameterizedTest
        @MethodSource("removeAllTestArgs")
        void removeAllTest(Object[] arr, boolean expected) {
            boolean actual = cut.removeAll(arr);
            Assertions.assertEquals(expected, actual);
        }
    }