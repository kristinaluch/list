package com.listi.LinkedList;

import com.listi.LinkedList.LinkedList2;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.List;

public class LinkedList2Test {
    LinkedList2 cut = new LinkedList2(new Integer[] {4, 19, 11, 68, 32, 45});

    @Test
    void sizeTest() {
        int expected = 6;
        int actual = (int) cut.size();
        Assertions.assertEquals(expected, actual);
    }

    static List<Arguments> getTestArgs(){
        return List.of(
                Arguments.arguments(3, 68),
                Arguments.arguments(0, 4)
        );
    }

    @ParameterizedTest
    @MethodSource("getTestArgs")
    void getTest(int index, int expected) {
        int actual = (int) cut.get(index);
        Assertions.assertEquals(expected, actual);
    }

    @Test
    void addTest() {
        boolean expected = false;
        boolean actual = cut.add(232);
        Assertions.assertEquals(expected, actual);
    }

    @Test
    void addFirstTest() {
        boolean expected = false;
        boolean actual = cut.addFirst(33);
        Assertions.assertEquals(expected, actual);
    }

    static List<Arguments> addTestArgs(){
        return List.of(
                Arguments.arguments(3, 55, false),
                Arguments.arguments(10, 32, false)
        );
    }

    @ParameterizedTest
    @MethodSource("addTestArgs")
    void add2Test(int index, int value, boolean expected) {
        boolean actual = cut.add(index, value);
        Assertions.assertEquals(expected, actual);
    }

    static List<Arguments> removeTestArgs(){
        return List.of(
                Arguments.arguments(13, false),
                Arguments.arguments(109, false)
        );
    }

    @ParameterizedTest
    @MethodSource("removeTestArgs")
    void removeTest(int number, boolean expected) {
        boolean actual = cut.remove(number);
        Assertions.assertEquals(expected, actual);
    }

    static List<Arguments> removeByIndexTestArgs(){
        return List.of(
                Arguments.arguments(5, 5),
                Arguments.arguments(47, -1)
        );
    }

    @ParameterizedTest
    @MethodSource("removeByIndexTestArgs")
    void removeByIndexTest(int index,  int expected) {
        int actual = (int) cut.removeByIndex(index);
        Assertions.assertEquals(expected, actual);
    }

    static List<Arguments> containsTestArgs(){
        return List.of(
                Arguments.arguments(0, false),
                Arguments.arguments(27, false)
        );
    }

    @ParameterizedTest
    @MethodSource("containsTestArgs")
    void containsTest(int number, boolean expected) {
        boolean actual = cut.contains(number);
        Assertions.assertEquals(expected, actual);
    }

    static List<Arguments> setTestArgs(){
        return List.of(
                Arguments.arguments(1, 13, true),
                Arguments.arguments(43, 28, false)
        );
    }

    @ParameterizedTest
    @MethodSource("setTestArgs")
    void setTest(int index, int value, boolean expected) {
        boolean actual = cut.set(index, value);
        Assertions.assertEquals(expected, actual);
    }

    @Test
    void toArrayTest() {
        Object[] expected = new Object[] {4, 19, 11, 68, 32, 45};
        Object[] actual = cut.toArray();
        Assertions.assertArrayEquals(expected, actual);
    }

    static List<Arguments> removeAllTestArgs(){
        return List.of(
                Arguments.arguments(new Object[] {13, 15, 23} , false),
                Arguments.arguments(new Object[] {4, 19, 11, 68, 32, 45}, true),
                Arguments.arguments(new Object[] {4, 32}, true)
        );
    }

    @ParameterizedTest
    @MethodSource("removeAllTestArgs")
    void removeAllTest(Object[] arr, boolean expected) {
        boolean actual = cut.removeAll(arr);
        Assertions.assertEquals(expected, actual);
    }
}