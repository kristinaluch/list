package com.listi.ArrayList;

import com.listi.ArrayList.ArrayList1;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.List;

class ArrayList1Test {
    ArrayList1 cut = new ArrayList1(new int[] {4, 19, 11, 68, 32, 45});

    @Test
    void sizeTest() {
        int expected = 6;
        int actual = cut.size();
        Assertions.assertEquals(expected, actual);
    }

    static List<Arguments> getTestArgs(){
        return List.of(
                Arguments.arguments(3, 68),
                Arguments.arguments(0, 4)
        );
    }

    @ParameterizedTest
    @MethodSource("getTestArgs")
    void getTest(int index, int expected) {
        int actual = cut.get(index);
        Assertions.assertEquals(expected, actual);
    }

    @Test
    void addTest() {
        boolean expected = true;
        boolean actual = cut.add(33);
        Assertions.assertEquals(expected, actual);
    }

    static List<Arguments> addTestArgs(){
        return List.of(
                Arguments.arguments(3, 23, true),
                Arguments.arguments(8, 213, false)
        );
    }

    @ParameterizedTest
    @MethodSource("addTestArgs")
    void add2Test(int index, int value, boolean expected) {
        boolean actual = cut.add(index, value);
        Assertions.assertEquals(expected, actual);
    }

    static List<Arguments> removeTestArgs(){
        return List.of(
                Arguments.arguments(4, true),
                Arguments.arguments(109, false)
        );
    }

    @ParameterizedTest
    @MethodSource("removeTestArgs")
    void removeTest(int number, boolean expected) {
        boolean actual = cut.remove(number);
        Assertions.assertEquals(expected, actual);
    }

    static List<Arguments> removeByIndexTestArgs(){
        return List.of(
                Arguments.arguments(4, 4),
                Arguments.arguments(214, -1)
        );
    }

    @ParameterizedTest
    @MethodSource("removeByIndexTestArgs")
    void removeByIndexTest(int index,  int expected) {
        int actual = cut.removeByIndex(index);
        Assertions.assertEquals(expected, actual);
    }

    static List<Arguments> containsTestArgs(){
        return List.of(
                Arguments.arguments(32, true),
                Arguments.arguments(888, false)
        );
    }

    @ParameterizedTest
    @MethodSource("containsTestArgs")
    void containsTest(int number, boolean expected) {
        boolean actual = cut.contains(number);
        Assertions.assertEquals(expected, actual);
    }

    static List<Arguments> setTestArgs(){
        return List.of(
                Arguments.arguments(3, 23, true),
                Arguments.arguments(2342,1231, false)
        );
    }

    @ParameterizedTest
    @MethodSource("setTestArgs")
    void setTest(int index, int value, boolean expected) {
        boolean actual = cut.set(index, value);
        Assertions.assertEquals(expected, actual);
    }

    @Test
    void toArrayTest() {
        int[] expected = new int[] {4, 19, 11, 68, 32, 45};
        int[] actual = cut.toArray();
        Assertions.assertArrayEquals(expected, actual);
    }

    static List<Arguments> removeAllTestArgs(){
        return List.of(
                Arguments.arguments(new int[] {4, 11, 444}, true),
                Arguments.arguments(new int[] {4, 19, 11, 68, 32, 45}, true),
                Arguments.arguments(new int[] {2, 423}, false)
        );
    }

    @ParameterizedTest
    @MethodSource("removeAllTestArgs")
    void removeAllTest(int[] arr, boolean expected) {
        boolean actual = cut.removeAll(arr);
        Assertions.assertEquals(expected, actual);
    }

}