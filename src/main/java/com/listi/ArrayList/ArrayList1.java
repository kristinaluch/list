package com.listi.ArrayList;

import java.util.Arrays;

public class ArrayList1 implements AList {
    private int capacity;
    private int[] array;

    public ArrayList1() {
        capacity = 10;
        array = new int[capacity];
    }

    public ArrayList1(int capacity) {
        this.capacity = capacity;
        array = new int[capacity];
    }

    public ArrayList1(int[] array) {
        this.array = array;
        capacity = array.length;
    }

    @Override
    public void clear() {
        array = new int[0];
    }

    @Override
    public int size() {
        return array.length;
    }

    @Override
    public int get(int index) {
        return array[index];
    }

    @Override
    public boolean add(int value) {
        int amount = 0;
        for (int i = 0; i < array.length; i++) {
            if((array[i]+"").equals("null")){
                array[i] = value;
                amount++;
                return true;
            }
        }
        if (amount == 0){
            int[] arr = new int[array.length*2];
            for (int i = 0; i < array.length; i++){
                arr[i] = array[i];
            }
            arr[array.length] = value;
            array = arr;
            return true;
        }
        return false;
    }

    @Override
    public boolean add(int index, int value) {
        if(index < array.length){
            int[] arr = new int[array.length+1];
            for(int i = 0; i < index; i++){
                arr[i] = array[i];
            }
            arr[index] = value;
            for(int i = index+1; i < array.length+1; i++){
                arr[i] = array[i-1];
            }
            array = arr;
            return true;
        }
        else {
            return false;
        }
    }

    @Override
    public boolean remove(int value) {
        int index = -1;

        for(int i = 0; i < array.length; i++){
            if(array[i] == value){
                index = i;
                break;
            }
        }

        if(index == -1){
            return false;
        }
        else {
            int[] arr= new int[array.length-1];
            for (int i = 0; i < index; i++){
                arr[i] = array[i];
            }
            for (int i = index+1; i < array.length; i++){
                arr[i-1] = array[i];
            }
            array = arr;
            return true;
        }
    }

    @Override
    public int removeByIndex(int index) {
        if(index >= array.length){
            return -1;
        }
        else {
            int [] arr = new int [array.length-1];
            for (int i = 0; i < index; i++){
                arr[i] = array[i];
            }
            for (int i = index+1; i < array.length; i++){
                arr[i-1] = array[i];
            }
            array = arr;
            return index;
        }
    }

    @Override
    public boolean contains(int value) {
        for (Integer number:array) {
            if (number == null){
                continue;
            }
            else {
                if(number == value){
                    return true;
                }
            }
        }
        return false;
    }

    @Override
    public boolean set(int index, int value) {
        if(index < array.length){
            array[index] = value;
            return true;
        }
        return false;
    }

    @Override
    public void print() {
        System.out.println(Arrays.toString(array));
    }

    @Override
    public int[] toArray() {
        int[] arr = new int[array.length];
        for(int i = 0; i < array.length; i++){
            arr[i] = array[i];
        }
        return arr;
    }

    @Override
    public boolean removeAll(int[] ar) {
        int[] arr = array;
        for(int i = 0; i < ar.length; i++){
            remove(ar[i]);
        }

        if(array != arr){
            return true;
        }
        return false;
    }
}
